package com.company;

import clases.CapacidadEndedudamiento;
import clases.Constantes;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String control;
        int ingMensuales;
        int gasfijos;
        int gastvariables;

        Scanner in = new Scanner(System.in);
        String entrada = "SI";
        while (entrada.equals("SI")) {
            System.out.println(Constantes.MensIngreso);
            ingMensuales = convertNumeric(in.next(), in, Constantes.MensIngreso);

            System.out.println("Ingrese sus gastos fijos");
            gasfijos = convertNumeric(in.next(), in, Constantes.MensgastFijos);

            System.out.println("Ingrese sus gastos variable");
            gastvariables = convertNumeric(in.next(), in, Constantes.MensgastVbles);


            CapacidadEndedudamiento capacidad = new CapacidadEndedudamiento();
            capacidad.setIngresosTotales(ingMensuales);
            capacidad.setGastosFijos(gasfijos);
            capacidad.setGastoVaribales(gastvariables);
            CapacidadEndedudamiento propiedades = new CapacidadEndedudamiento();
            System.out.println(capacidad.getCapacidadEndeudamiento());

            System.out.println(Constantes.Menscontinuar);
            control = in.next();
            if (continuar(control) == true) {
                if (control.equals("N")) {
                    entrada = "NO";
                }
            } else
                return;

        }
        in.close();
    }


    private static boolean continuar(String decision) {

        if (decision.equals("S") || decision.equals("N")) {
            return true;
        }
        return false;
    }


    private static boolean isNumeric(String entradaDatos) {
        try {
            Integer.parseInt(entradaDatos);
            return true;
        } catch (NumberFormatException exception) {
            return false;
        }
    }

    private static int convertNumeric(String value, Scanner in, String mensaje) {

        while (isNumeric(value) != true) {
            System.out.println(Constantes.MensError + "\n" + mensaje);
            value = in.next();
        }
        return Integer.parseInt(value);
    }

}
