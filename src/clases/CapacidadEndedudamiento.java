package clases;

public class CapacidadEndedudamiento {
    Integer ingresosTotales;
    Integer gastosFijos;
    Integer gastoVaribales;
    final double POR_FIJO = 0.35;

    double calculo_Capacidad;


    //Metodos getter and setter para la clase

    public void setIngresosTotales(Integer ingresosTotales) {
        this.ingresosTotales = ingresosTotales;
    }

    public void setGastosFijos(Integer gastosFijos) {

        this.gastosFijos = gastosFijos;
    }

    public void setGastoVaribales(Integer gastoVaribales) {

        this.gastoVaribales = gastoVaribales;
    }

    public Integer getIngresosTotales() {

        return ingresosTotales;
    }

    public Integer getGastosFijos() {
        return gastosFijos;
    }

    public Integer getGastoVaribales() {
        return gastoVaribales;
    }

    public double getPOR_FIJO() {
        return POR_FIJO;
    }


    public String propiedades() {
        return "Las propiedades son: " + '\n' + "Ingresos mensuales: " + getIngresosTotales() + '\n'
                + "Los gastos fijos son: " + getGastosFijos() + '\n'
                + "los Gastos Variables son:" + getGastoVaribales() + '\n';
    }    //Construye un metodo que retorne una cadena con las propiedades de la clase


    public String getCapacidadEndeudamiento() {
        // retornar la capacidad de endeudamiento puede ser una cadena con el valor
        calculo_Capacidad = ((getIngresosTotales() - (getGastosFijos() + getGastoVaribales())) * getPOR_FIJO());
        return "La Capacidad de endeudamiento es:" + calculo_Capacidad;
    }
}
