package clases;

import java.lang.annotation.Target;
import java.util.Scanner;

public class Constantes {
public static final String MensIngreso = "Ingrese el valor de los ingresos Mensuales: ";
public static final String MensError = "El valor ingresado no es númerico";
public static final String MensgastFijos = "Ingrese el valor de sus gastos fijos:  ";
public static final String MensgastVbles = "Ingrese el valor de sus gastos Variable: ";
public static final String Menscontinuar = "Si desea realizar de nuevo el calculo ingrese S de los contrario ingrese N";
public static final String Mensdecison = "Debes ingresar el caracter S o N";

}
